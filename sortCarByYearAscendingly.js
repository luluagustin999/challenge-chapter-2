function sortCarByYearAscendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log('Before Ascending :', cars);

  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const result = [...cars]; // mengambil nilai array of object cars

  // Tulis code-mu disini

  //result sort menyorting array dari cars, terdapat local function dengan parameter a dan b //

  result.sort(function (a, b) {

    let yearA = a.year,
      yearB = b.year;
    const hasil = yearA - yearB;
    return hasil;

  });

  //menampilkan hasil ascending
  console.log('Result Ascending :', result);

  // Rubah code ini dengan array hasil sorting secara descending
  return result;
}