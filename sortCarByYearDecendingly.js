function sortCarByYearDescendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log('Before Descending :', cars);

  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const result = [...cars];

  //tulis code mu disini

  //result sort menyorting array dari cars, terdapat local function dengan parameter a dan b //

  result.sort(function (a, b) {

    let yearA = a.year,
      yearB = b.year;
    const hasil = yearB - yearA;
    return hasil;

  });

  //menampilkan hasil result descending
  console.log('Result Descending :', result);

  // Rubah code ini dengan array hasil sorting secara ascending
  return result;
}